## Prerequisites
- Azure AzureCLI (https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-windows?view=azure-cli-latest&tabs=azure-cli)
- Pulumi CLI (https://www.pulumi.com/docs/get-started/install/)
- Node (https://nodejs.org/en/download/)
- git (https://git-scm.com/download/win)

## Configuaration File (Pulumi.prod.yaml)
```
config:
  basic-auth-proxy:authUser: hufcor-viewer
  basic-auth-proxy:dnsPrefix: fp-dashboard-hufcor
  basic-auth-proxy:location: centralus
  basic-auth-proxy:namePrefix: fp-hufcor-prod
  basic-auth-proxy:shortNamePrefix: fphufprod
  basic-auth-proxy:upstream: https://dashboard.cloud.freedom-platform.io
```

## Deployment

```
git clone https://gitlab.com/freedom-platform-integration/basic-auth-proxy.git
cd basic-auth-proxy\deploy\azure\pulumi

npm i -g @pulumi/pulumi
npm install

pulumi login file://~

cp Pulumi.sample.yaml Pulumi.prod.yaml
notepad.exe Pulumi.prod.yaml

pulumi stack init prod
pulumi config set azure:location
pulumi config set authPassword

pulumi up
```

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.1.0](https://gitlab.com/freedom-platform-integration/basic-auth-proxy/compare/v2.0.0...v2.1.0) (2020-08-26)


### Features

* add azure deployment via pulumi ([c9ced52](https://gitlab.com/freedom-platform-integration/basic-auth-proxy/commit/c9ced52e8d0e7b178245ea20b6728041bc9ff81e))

## [2.0.0](https://gitlab.com/freedom-platform-integration/basic-auth-proxy/compare/v1.2.0...v2.0.0) (2020-08-08)


### ⚠ BREAKING CHANGES

* UPSTREAM_HOST and UPSTREAM_PROTO have been replaced with a single UPSTREAM
configuration variable

### Bug Fixes

* disambiguate upstream log message ([74501b9](https://gitlab.com/freedom-platform-integration/basic-auth-proxy/commit/74501b91f24dad1a5493070e5632961c53d64ea9))


* simplify upstream configuration ([129f826](https://gitlab.com/freedom-platform-integration/basic-auth-proxy/commit/129f826158be4d9af2e75d5e376428b3195ae872))

## [1.2.0](https://gitlab.com/freedom-platform/basic-auth-proxy/compare/v1.1.3...v1.2.0) (2020-08-07)


### Features

* add cheeky powered-by header ([070cf2a](https://gitlab.com/freedom-platform/basic-auth-proxy/commit/070cf2afef3c5aee83cb9c7bef9bf9af9f860de7))


### Bug Fixes

* disable basic auth middlewaer when credentials are overridden ([d5f3507](https://gitlab.com/freedom-platform/basic-auth-proxy/commit/d5f35073a42eed52090fffff6bc686196449709f))

### [1.1.3](https://gitlab.com/freedom-platform/basic-auth-proxy/compare/v1.1.2...v1.1.3) (2020-08-06)


### Bug Fixes

* remove challenge when credentials are overridden ([4177ffe](https://gitlab.com/freedom-platform/basic-auth-proxy/commit/4177ffe7d6786dbd4cd36c05ca5d7fd25a732cdd))

### [1.1.2](https://gitlab.com/freedom-platform/basic-auth-proxy/compare/v1.1.1...v1.1.2) (2020-08-06)

### [1.1.1](https://gitlab.com/freedom-platform/basic-auth-proxy/compare/v1.1.0...v1.1.1) (2020-08-06)


### Bug Fixes

* log override user ([1697025](https://gitlab.com/freedom-platform/basic-auth-proxy/commit/1697025915e755ce541a582cf70333503fbc7dc4))

## [1.1.0](https://gitlab.com/freedom-platform/basic-auth-proxy/compare/v1.0.8...v1.1.0) (2020-08-05)


### Features

* allow override credentials ([55f17a6](https://gitlab.com/freedom-platform/basic-auth-proxy/commit/55f17a6d42560599cb4534e5d5f35c7be74c653c))

### [1.0.8](https://gitlab.com/freedom-platform/basic-auth-proxy/compare/v1.0.7...v1.0.8) (2020-08-05)

### [1.0.7](https://gitlab.com/freedom-platform/basic-auth-proxy/compare/v1.0.6...v1.0.7) (2020-08-05)


### Bug Fixes

* force https upstream ([633bae4](https://gitlab.com/freedom-platform/basic-auth-proxy/commit/633bae44d4d0e006df5e91c054088fdb3f115aa6))

### [1.0.6](https://gitlab.com/freedom-platform/basic-auth-proxy/compare/v1.0.5...v1.0.6) (2020-08-05)

### [1.0.5](https://gitlab.com/freedom-platform/basic-auth-proxy/compare/v1.0.4...v1.0.5) (2020-08-05)

### [1.0.4](https://gitlab.com/freedom-platform/basic-auth-proxy/compare/v1.0.3...v1.0.4) (2020-08-05)

### [1.0.3](https://gitlab.com/freedom-platform/basic-auth-proxy/compare/v1.0.2...v1.0.3) (2020-08-05)

### [1.0.2](https://gitlab.com/freedom-platform/basic-auth-proxy/compare/v1.0.1...v1.0.2) (2020-08-05)

### 1.0.1 (2020-08-05)


### Bug Fixes

* add commitlint config to package.json ([f39dc02](https://gitlab.com/freedom-platform/basic-auth-proxy/commit/f39dc02408a8947bd8f352abc9e13b6e7f1cc184))

# Pulumi stack for Basic Auth Proxy with auto SSL (via [Caddy](https://caddyserver.com/))

[![Deploy](https://get.pulumi.com/new/button.svg)](https://app.pulumi.com/new)

## Prerequisites

In order to deploy this integration, you will need

- The Microsoft Azure CLI. Get it here - https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest
- The Pulumi CLI. Get it here - https://www.pulumi.com/docs/get-started/install/

## Prepearing For Deployment 

1.  Choose a storage location for your Pulumi state (pick one):
    - \[**Recommended**\] Using Pulumi Cloud (create an account at https://www.pulumi.com/)
        ```console
        foo@bar:~$ pulumi login
        ```

    - Using a new Microsoft Azure Blob Storage container
        ```console
        foo@bar:~$ PULIMI_STATE_RG="pulumi-state-rg"
        foo@bar:~$ PULIMI_STATE_STORAGE="mypulumistate"
        foo@bar:~$ PULIMI_STATE_CONTAINER="pulumi-state"
        foo@bar:~$ az login
        foo@bar:~$ az group create --name $PULIMI_STATE_RG --location EastUS
        foo@bar:~$ az storage account create --name $PULIMI_STATE_STORAGE --resource-group $PULIMI_STATE_RG --location EastUS --sku Standard_ZRS --encryption-services blob
        foo@bar:~$ export AZURE_STORAGE_ACCOUNT=$PULIMI_STATE_STORAGE
        foo@bar:~$ KEYS=$(az storage account keys list --account-name $AZURE_STORAGE_ACCOUNT --resource-group $PULIMI_STATE_RG --output json)
        foo@bar:~$ export AZURE_STORAGE_KEY=$(echo $KEYS | jq -r .[0].value)
        foo@bar:~$ az storage container create --name $PULIMI_STATE_CONTAINER --fail-on-exist 
        foo@bar:~$ pulumi login azblob://$PULIMI_STATE_CONTAINER
        ```
    
    - Using an existing Microsoft Azure Blob Storage container
        ```console
        foo@bar:~$ PULIMI_STATE_CONTAINER="pulumi-state"
        foo@bar:~$ export AZURE_STORAGE_ACCOUNT="mypulumistate"
        foo@bar:~$ az login
        foo@bar:~$ KEYS=$(az storage account keys list --account-name $AZURE_STORAGE_ACCOUNT --resource-group $PULIMI_STATE_RG --output json)
        foo@bar:~$ export AZURE_STORAGE_KEY=$(echo $KEYS | jq -r .[0].value)
        foo@bar:~$ az storage container create --name $PULIMI_STATE_CONTAINER --fail-on-exist 
        foo@bar:~$ pulumi login azblob://$PULIMI_STATE_CONTAINER
        ```

      - Using a local folder
          ```console
          foo@bar:~$ pulumi login file://~
          ```

## Deploying The Stack

1. If you have not already done so, log-in to Azure:
    ```console
    foo@bar:~$ az login
    ```

1.  Create a new stack (you may use any name you like, however, `dev`, `test`, and `prod` are best-practice examples):

    ```console
    foo@bar:~$ STACK_NAME=<dev|test|prod>
    foo@bar:~$ cp Pulumi.sample.yaml Pulumi.${STACK_NAME}.yaml
    foo@bar:~$ vi Pulumi.${STACK_NAME}.yaml
    foo@bar:~$ pulumi stack init $STACK_NAME
    ```

1.  Configure Azure location:

    ```console
    foo@bar:~$ pulumi config set azure:location
    value: <location>
    ```

1.  Install NPM dependencies:

    ```console
    foo@bar:~$ npm install
    ```

1.  Run `pulumi up` to preview and deploy changes:

    ``` console
    foo@bar:~$ pulumi up
    Previewing update (dev):
    ...

    Updating (dev):
    ...
    Resources:
       + 15 created
    Duration: 5m54s
    ```

1.  Note the endpoint URL

    ```console
    foo@bar:~$ pulumi stack output endpoint
    https://fp-dev-proxy-dashboard-a23d8e55.eastus.azurecontainer.io
    ```

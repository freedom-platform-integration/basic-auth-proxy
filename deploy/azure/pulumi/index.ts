import * as azure from '@pulumi/azure';
import * as pulumi from '@pulumi/pulumi';

import { generator } from 'rand-token';
const generateToken = () => generator({
  chars: 'abcdef0123456789',
}).generate(8);

// Configuration
const env = pulumi.getStack() || 'dev';
const envConfig = new pulumi.Config();

const namePrefix = envConfig.get('namePrefix') || `fp-${env}-proxy`;
const shortNamePrefix = envConfig.get('shortNamePrefix') || `fp${env}prxy`;
if (shortNamePrefix.length > 10) {
  throw new Error(`shortNamePrefix '${shortNamePrefix}' exceeds the maximum length of 10 characters`);
}

const resourceGroupName = envConfig.get('resourceGroupName') || ''
const storageAccountName = envConfig.get('storageAccountName') || ''
const shareName = envConfig.get('shareName') || ''

const upstream = envConfig.require('upstream');
const authUser = envConfig.get('authUser') || '';
const authPassword = envConfig.get('authPassword') || '';

const commonTags = {
  environment: env === 'prod' ? 'Production' : 'Development',
  app: pulumi.getProject(),
};

const dnsPrefix = (env === 'prod' && envConfig.get('dnsPrefix')) ? envConfig.get('dnsPrefix') : `${namePrefix}-dashboard-${generateToken()}`;

// Create or get resource group
const resourceGroup = !resourceGroupName ? new azure.core.ResourceGroup(`${namePrefix}-rg`, {
  tags: commonTags,
}) : pulumi.output(azure.core.getResourceGroup({
  name: resourceGroupName,
}));

// Create or get storage account
const storageAccount = !storageAccountName ? new azure.storage.Account(`${shortNamePrefix}store`, {
  resourceGroupName: resourceGroup.name,
  location: resourceGroup.location,
  accountTier: 'Standard',
  accountReplicationType: 'LRS',
  tags: commonTags,
}) : pulumi.output(azure.storage.getAccount({
  name: storageAccountName
}));

// Create file share in storage account
const tlsShare = new azure.storage.Share(shareName || 'caddytls', {
  storageAccountName: storageAccount.name,
  quota: 1,
});

const imageTag = envConfig.get('imageTag') || 'latest';
const imageRegistry = envConfig.get('imageRegistry') || 'registry.gitlab.com/freedom-platform-integration/basic-auth-proxy'

const fqdn = pulumi.interpolate`${dnsPrefix}.${resourceGroup.location}.azurecontainer.io`;

const containerGroup = new azure.containerservice.Group(`${namePrefix}-container`, {
  location: resourceGroup.location,
  resourceGroupName: resourceGroup.name,
  restartPolicy: 'OnFailure',
  ipAddressType: 'public',
  dnsNameLabel: dnsPrefix,
  osType: 'Linux',
  containers: [{
      name: 'basic-auth-proxy',
      image: `${imageRegistry}:${imageTag}`,
      ports: [{
        port: 3000,
        protocol: 'TCP',
      }],
      environmentVariables: authUser ? {
        UPSTREAM: upstream,
        AUTH_USER: authUser,
      } : {
        UPSTREAM: upstream,
      },
      secureEnvironmentVariables: authPassword ? {
        AUTH_PASS: authPassword,
      } : {},
      livenessProbe: {
        httpGets:[{
          path: '/_health/liveness',
          port: 8888,
        }]
      },
      readinessProbe: {
        httpGets:[{
          path: '/_health/readiness',
          port: 8888,
        }]
      },
      cpu: 0.5,
      memory: 0.3,
    },{
      name: 'caddy',
      image: `${imageRegistry}:${imageTag}-caddy`,
      commands: [
        'caddy',
        'run',
        '--config',
        envConfig.get('acmeIssuer') === 'staging' ? '/etc/caddy/Caddyfile.staging' : '/etc/caddy/Caddyfile',
      ],
      ports: [{
        port: 80,
        protocol: 'TCP',
      }, {
        port: 443,
        protocol: 'TCP',
      }],
      environmentVariables: {
        UPSTREAM: 'http://localhost:3000',
        SITE_ADDRESS: fqdn,
        ALLOW_CIDR: envConfig.get('allowedSources') || '0.0.0.0/0',
      },
      volumes: [{
        name: 'caddy-tls',
        mountPath: '/data',
        shareName: tlsShare.name,
        storageAccountName: storageAccount.name,
        storageAccountKey: storageAccount.primaryAccessKey,
      }],
      cpu: 0.5,
      memory: 0.3,
    },
  ],
  tags: commonTags,
});

export const endpoint = pulumi.interpolate`https://${containerGroup.fqdn}`;

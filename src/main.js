const pino = require('pino');
const http = require('http');

const { createTerminus } = require('@godaddy/terminus');

const App = require('./app');

const logLevel = process.env.LOG_LEVEL || 'info';
const logger = pino({ base: { service: 'basic-auth-proxy' }, level: logLevel });

const port = process.env.PORT || 3000;
const app = App({ logger: logger.child({ module: 'app' }) });

const healthcheckPort = process.env.HEALTHCHECK_PORT || 8888;

const shutdownDelay = process.env.SHUTDOWN_DELAY || 5000;

const onSignal = async () => {
  logger.info('starting cleanup');
  await Promise.all([]);
};

const onShutdown = () => {
  logger.info('cleanup finished, server is shutting down');
};

const onError = error => {
  if (error.syscall !== 'listen') {
    throw error;
  }

  // Handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      logger.error({ port }, 'port requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      logger.error({ port }, 'port is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
};

const beforeShutdown = async () => {
  logger.info(`waiting ${shutdownDelay}ms before shutdown`);
  return new Promise(resolve => {
    setTimeout(resolve, shutdownDelay);
  });
};

const healthCheck = async () => {
  return 'ok';
};

const terminusOptions = {
  // health check options
  healthChecks: {
    healthChecks: {
      '/_health/liveness': healthCheck,
      '/_health/readiness': healthCheck,
    },
    verbatim: true,
  },

  // cleanup options
  timeout: 1000,
  signals: ['SIGINT', 'SIGHUP', 'SIGTERM'],
  beforeShutdown,
  onSignal,
  onShutdown,

  // both
  logger: (msg, err) => logger.error({ err }, msg),
};

const terminusServer = http.createServer((request, response) => {
  response.end('ok');
});
createTerminus(terminusServer, terminusOptions);
terminusServer.listen(healthcheckPort, () => logger.info({ port: healthcheckPort }, 'terminus listening'));

const server = http.createServer(app);
server.listen(port, () => logger.info({ port }, 'listening'));
server.on('error', onError);

const assert = require('assert');
const URL = require('url');
const express = require('express');

const proxy = require('express-http-proxy');
const basicAuth = require('express-basic-auth');

const poweredBy = (req, res, next) => {
  res.setHeader('X-Powered-By', 'Diet Pepsi');
  next();
};

const App = ({ logger }) => {
  const app = express();

  if (process.env.NODE_ENV === 'dev') {
    app.use(require('express-pino-logger')({
      logger,
    }));
  }

  app.use(poweredBy);

  const upstream = process.env.UPSTREAM;
  assert(upstream, 'UPSTREAM must be set');

  const upstreamUrl = URL.parse(upstream);
  logger.info({
    upstream,
    host: upstreamUrl.hostname,
    port: upstreamUrl.port,
    protocol: upstreamUrl.protocol,
  }, 'set upstream');

  const forceSSL = upstreamUrl.protocol === 'https:';

  const overrideUser = process.env.AUTH_USER;
  const overridePassword = process.env.AUTH_PASS;

  if (overrideUser) {
    assert(overridePassword, 'AUTH_PASS must be set if AUTH_USER is set');
    logger.info({ user: overrideUser }, 'set override credentials');
  } else {
    app.use(basicAuth({
      authorizer: () => true,
      challenge: true,
      realm: process.env.CHALLENGE_REALM || 'basic-auth-proxy',
    }));
  }

  app.use('/', proxy(upstreamUrl.host, {
    https: forceSSL,
    proxyReqOptDecorator: (proxyReqOpts) => {
      logger.info({ authentication: !!proxyReqOpts.headers.authorization, url: proxyReqOpts.path }, 'proxying request');
      if (!proxyReqOpts.headers.authorization) {
        const auth = proxyReqOpts.auth || {};
        const user = overrideUser || auth.user;
        const password = overridePassword || auth.password;
        if (!user || !password) {
          throw new Error('invalid credentials');
        }
        const digest = Buffer.from(`${user}:${password}`).toString('base64');
        proxyReqOpts.headers.Authorization = `Basic ${digest}`; // eslint-disable-line no-param-reassign
      }
      return proxyReqOpts;
    },
  }));

  return app;
};

module.exports = App;

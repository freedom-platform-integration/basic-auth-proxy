FROM node:lts-alpine

WORKDIR /app

COPY package*.json ./

RUN npm ci --only=production

COPY src/ ./

ENV NODE_ENV production

CMD [ "node", "main.js" ]

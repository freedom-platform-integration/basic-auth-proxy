# Basic Authenticating Proxy

This service will proxy requests to a backend that accepts HTTP Basic authentication, but does not send a WWW-Authenticate header with 401 responses (e.g. Grafana, when auto OAuth is enabled).

## Configuration

Set the UPSTREAM environment variable to the upstream protocol://host that requests should be forwarded to (e.g. https://foo.example.com). If protocol is omitted, it will be inferred from the downstream request.

To use a static set of credentials, set AUTH_USER and AUTH_PASS accordingly

To override the HTTP service port, set the PORT environment variable (default is 3000)

## Execution

Run the service in a docker container:

```console
foo@bar:~$ docker run -p 3000:3000 -e UPSTREAM=https://foo.example.com  registry.gitlab.com/freedom-platform-integration/basic-auth-proxy:latest
```

Run the service locally

```console
foo@bar:~$ UPSTREAM=https://foo.example.com node main.js
  ```

Run the service locally (development)

```console
foo@bar:~$ UPSTREAM=https://foo.example.com npm start
```

[Run the service in Azure (with auto SSL)](deploy/azure/pulumi/README.md)
